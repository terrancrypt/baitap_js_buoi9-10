var DSNV = "DSNV";
var arrNv = [];

// Lấy dữ liệu từ Local Storage
var arrNvJson = localStorage.getItem(DSNV);
// Nếu có dữ liệu từ Local thì sẽ render ra màn hình
if (arrNvJson != null) {
  var arrNhanVien = JSON.parse(arrNvJson);
  arrNv = arrNhanVien.map(function (item) {
    return (nv = new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    ));
  });
  renderNhanVien(arrNv);

  // arrNv = JSON.parse(arrNvJson);
  // renderNhanVien(arrNv);
}

// Thêm nhân viên
function themNhanVien() {
  // Lấy thông tin người dùng từ giao diện và push vào mảng arrNv
  var nv = layThongTinTuGiaoDien();

  // Validation
  var isEmpty = true;
  isEmpty = kiemTraFormTrong(nv.taiKhoan, "tbTKNV");
  isEmpty = isEmpty & kiemTraFormTrong(nv.hoTen, "tbTen");
  isEmpty = isEmpty & kiemTraFormTrong(nv.email, "tbEmail");
  isEmpty = isEmpty & kiemTraFormTrong(nv.matKhau, "tbMatKhau");
  isEmpty = isEmpty & kiemTraFormTrong(nv.ngayLam, "tbNgay");
  isEmpty = isEmpty & kiemTraFormTrong(nv.luongCoBan, "tbLuongCB");
  isEmpty = isEmpty & kiemTraFormTrong(nv.gioLam, "tbGiolam");

  // Nếu tất cả các biến đều đúng (tức là có giá trị trong mọi trường nhập thì chương trình sẽ tiếp tục chạy)
  if (isEmpty) {
    // Tạo biến chứa giá trị isValid
    var isValid = true;
    isValid =
      kiemTraTrungLap(nv.taiKhoan, arrNv) &&
      kiemTraTaiKhoan(nv.taiKhoan, "tbTKNV", 4, 6);
    isValid = isValid & kiemTraTenNV(nv.hoTen);
    isValid = isValid & kiemTraEmail(nv.email);
    isValid = isValid & kiemTraPass(nv.matKhau);
    isValid = isValid & kiemTraLuong(nv.luongCoBan);
    isValid = isValid & kiemTraChucVu(nv.chucVu);
    isValid = isValid & kiemTraGioLam(nv.gioLam);
    // Nếu biến này vẫn đúng thì sẽ thêm nhân viên như bình thường
    if (isValid) {
      arrNv.push(nv);
      // Lưu vào Local Storage
      var arrNvJson = JSON.stringify(arrNv);
      localStorage.setItem(DSNV, arrNvJson);
      // Xuất thông tin ra màn hình
      renderNhanVien(arrNv);
    }
  }
}

// Xóa người dùng
function xoaNhanVien(taiKhoan) {
  //  tìm vị trí
  var viTri = timKiemViTri(taiKhoan, arrNv);
  if (viTri != -1) {
    arrNv.splice(viTri, 1);
    // render lại layout sau khi xóa thành công
    renderNhanVien(arrNv);
    // Lưu lại vào LocalStorage
    var arrNvJson = JSON.stringify(arrNv);
    localStorage.setItem(DSNV, arrNvJson);
  }
}

// Sửa người dùng
function suaNguoiDung(taiKhoan) {
  var viTri = timKiemViTri(taiKhoan, arrNv);
  if (viTri == -1) {
    return;
  }
  var nv = arrNv[viTri];
  showThongTinLenForm(nv);
}

// Cập nhật thông tin người dùng
function capNhatThongTin() {
  var nv = layThongTinTuGiaoDien();
  var viTri = timKiemViTri(nv.taiKhoan, arrNv);
  console.log(viTri);

  // Validation
  var isEmpty = true;
  isEmpty = kiemTraFormTrong(nv.taiKhoan, "tbTKNV");
  isEmpty = isEmpty & kiemTraFormTrong(nv.hoTen, "tbTen");
  isEmpty = isEmpty & kiemTraFormTrong(nv.email, "tbEmail");
  isEmpty = isEmpty & kiemTraFormTrong(nv.matKhau, "tbMatKhau");
  isEmpty = isEmpty & kiemTraFormTrong(nv.ngayLam, "tbNgay");
  isEmpty = isEmpty & kiemTraFormTrong(nv.luongCoBan, "tbLuongCB");
  isEmpty = isEmpty & kiemTraFormTrong(nv.gioLam, "tbGiolam");

  // Nếu tất cả các biến đều đúng (tức là có giá trị trong mọi trường nhập thì chương trình sẽ tiếp tục chạy)
  if (isEmpty) {
    // Tạo biến chứa giá trị isValid
    var isValid = true;
    isValid =
      //  kiemTraTrungLap(nv.taiKhoan, arrNv) &&
      kiemTraTaiKhoan(nv.taiKhoan, "tbTKNV", 4, 6);
    isValid = isValid & kiemTraTenNV(nv.hoTen);
    isValid = isValid & kiemTraEmail(nv.email);
    isValid = isValid & kiemTraPass(nv.matKhau);
    isValid = isValid & kiemTraLuong(nv.luongCoBan);
    isValid = isValid & kiemTraChucVu(nv.chucVu);
    isValid = isValid & kiemTraGioLam(nv.gioLam);
    // Nếu biến này vẫn đúng thì sẽ thêm nhân viên như bình thường
    if (isValid) {
      if (viTri != -1) {
        arrNv[viTri] = nv;
      }
      // Lưu vào Local Storage
      var arrNvJson = JSON.stringify(arrNv);
      localStorage.setItem(DSNV, arrNvJson);
      // Xuất thông tin ra màn hình
      renderNhanVien(arrNv);
    }
  }

  // if (viTri != -1){
  //   arrNv[viTri] = nv;
  //   renderNhanVien(arrNv);
  // }
}
