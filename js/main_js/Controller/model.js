function NhanVien(
    _taiKhoan,
    _hoTen,
    _email,
    _matKhau,
    _ngayLam,
    _luongCoBan,
    _chucVu,
    _gioLam
){
    this.taiKhoan = _taiKhoan;
    this.hoTen = _hoTen;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngayLam = _ngayLam;
    this.luongCoBan = _luongCoBan;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = function (){
        var tongLuong = 0;
        var luongCB = Number(this.luongCoBan);
        if (this.chucVu == 'Sếp'){
            return tongLuong = luongCB * 3;
        }else if(this.chucVu == 'Trưởng phòng'){
            return tongLuong = luongCB * 2;
        }else{
            return luongCB;
        }
    };
    this.xepLoai = function (){
        var xepLoai = "";
        if (this.gioLam >= 192){
            return xepLoai = "Xuất sắc";
        }else if (this.gioLam >= 176){
            return xepLoai = "Giỏi";
        }else if (this.gioLam >= 160){
            return xepLoai = "Khá";
        }else{
            return xepLoai = "Trung bình";
        }
    }
} 