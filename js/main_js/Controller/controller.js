function layThongTinTuGiaoDien() {
  //Lấy thông tin người dùng nhập từ giao diện
  var _taiKhoan = document.getElementById("tknv").value;
  var _hoTen = document.getElementById("name").value;
  var _email = document.getElementById("email").value;
  var _matKhau = document.getElementById("password").value;
  var _ngayLam = document.getElementById("datepicker").value;
  var _luongCoBan = document.getElementById("luongCB").value;
  var _chucVu = document.getElementById("chucvu").value;
  var _gioLam = document.getElementById("gioLam").value;

  var nhanVien = new NhanVien(
    _taiKhoan,
    _hoTen,
    _email,
    _matKhau,
    _ngayLam,
    _luongCoBan,
    _chucVu,
    _gioLam
  );

  return nhanVien;
}

function renderNhanVien(Arr) {
  // Render dssv ta table
  var contentHTML = "";
  for (var index = 0; index < Arr.length; index++) {
    var item = Arr[index];
    var contentTr = `
      <tr>
         <td> ${item.taiKhoan} </td>
         <td> ${item.hoTen} </td>
         <td> ${item.email} </td>
         <td> ${item.ngayLam} </td>
         <td> ${item.chucVu} </td>
         <td> ${item
           .tongLuong()
           .toLocaleString("it-IT", {
             style: "currency",
             currency: "VND",
           })} </td>
         <td> ${item.xepLoai()} </td>
         <td> <button class="btn btn-danger" onclick="xoaNhanVien('${
           item.taiKhoan
         }')">Xóa</button>
         <button class="btn btn-primary my-1" data-toggle="modal" data-target="#myModal" onclick="suaNguoiDung('${
          item.taiKhoan
        }')">Sửa</button>
          </td>
      </tr>
      `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(taiKhoan, arr) {
  var viTri = -1;
  for (var index = 0; index < arr.length; index++) {
    var nv = arr[index];
    if (nv.taiKhoan == taiKhoan) {
      viTri = index;
      break;
    }
  }

  return viTri;
}

function showThongTinLenForm(nv){
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
