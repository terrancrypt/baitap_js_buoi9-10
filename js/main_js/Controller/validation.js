// Kiểm tra trùng lặp
function kiemTraTrungLap(taiKhoan, array) {
  // Xác định vị trí của phần tử trong mảng
  var index = array.findIndex(function (item) {
    return taiKhoan == item.taiKhoan;
  });
  if (index == -1) {
    // Nếu index = -1 có nghĩa là không tìm thấy một giá trị nào bị trùng, xóa thông báo lỗi nếu có
    document.getElementById("tbTKNV").innerText = "";
    return true;
  } else {
    // Nếu tìm thấy thì cho hiện thẻ span
    document.getElementById("tbTKNV").style.display = "block";
    document.getElementById("tbTKNV").innerText = "Tài khoản đã tồn tại";
    return false;
  }
}

// Kiểm tra form có trống trường nào không
function kiemTraFormTrong(value, idErr) {
  var regex = /(^$)/;
  // var regex = /(^[0-9]+$|^$|^\s$)/;
  var isEmpty = regex.test(value);
  // Test nếu form trống thì sẽ trả về kết quả hiển thị lên màn hình
  if (isEmpty) {
    document.getElementById(idErr).style.display = "block";
    document.getElementById(idErr).innerText = "Không được để trống trường này";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

// Kiểm tra tài khoản: tài khoản từ 4-6 ký tự và không được để trống
function kiemTraTaiKhoan(value, idErr, min, max) {
  // Kiểm tra có phải là số hay không và không được bỏ trống
  var reg = /^-?\d+\.?\d*$/;
  var isNumber = reg.test(value);
  if (isNumber) {
    // lấy ra độ dài của giá trị mảng nhập vào
    var length = value.length;
    // nếu độ dài bé hơn min hoặc lớn hơn max thì sẽ in ra màn hình cảnh báo, còn nếu người dùng đã nhập đúng thì sẽ xóa dòng thông báo cũ đi
    if (length < min || length > max) {
      document.getElementById(
        idErr
      ).innerText = `Độ dài phải từ ${min} đến ${max} kí tự`;
      document.getElementById(idErr).style.display = "block";
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  } else {
    document.getElementById("tbTKNV").innerText = "Tài khoản chỉ được nhập số";
    document.getElementById(idErr).style.display = "block";
    return false;
  }
}

// Kiểm tra tên nhân viên
function kiemTraTenNV(value) {
  let reg = /^([a-zA-Z ]){2,30}$/;
  var isName = reg.test(value);
  if (isName) {
    document.getElementById("tbTen").innerText = "";
    return true;
  } else {
    document.getElementById("tbTen").innerText = "Tên nhân viên phải là chữ";
    document.getElementById("tbTen").style.display = "block";
    return false;
  }
}

// Kiểm tra email
function kiemTraEmail(value) {
  const reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = reg.test(value);
  if (isEmail) {
    document.getElementById("tbEmail").innerText = "";
    return true;
  } else {
    document.getElementById("tbEmail").innerText = "Email không hợp lệ";
    document.getElementById("tbEmail").style.display = "block";
  }
}

// Kiểm tra password
function kiemTraPass(value) {
  var reg =
    /(?=^.{6,10}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
  var isPassWord = reg.test(value);
  if (isPassWord) {
    document.getElementById("tbMatKhau").innerText = "";
    return true;
  } else {
    document.getElementById("tbMatKhau").innerText =
      "Mật khẩu từ 6 đến 10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
    document.getElementById("tbMatKhau").style.display = "block";
    return false;
  }
}

// Kiểm tra lương
function kiemTraLuong(value) {
  var reg = /^-?\d+\.?\d*$/;
  var isNumber = reg.test(value);
  var soLuong = Number(value);
  if (isNumber) {
    if (soLuong >= 1000000 && soLuong <= 20000000) {
      document.getElementById("tbLuongCB").innerText = "";
      return true;
    } else {
      document.getElementById("tbLuongCB").innerText =
        "Lương cơ bản từ 1.000.000đ đến 20.000.000đ";
      document.getElementById("tbLuongCB").style.display = "block";
      return false;
    }
  }else{
    document.getElementById('tbLuongCB').style.display = "block";
    document.getElementById("tbLuongCB").innerText =
    "Lương chỉ được nhập số";
    return false;
  }
}

// Kiểm tra chức vụ
function kiemTraChucVu(value){
  if (value == 1){
    document.getElementById('tbChucVu').style.display = "block";
    document.getElementById("tbChucVu").innerText =
    "Vui lòng chọn chức vụ";
    return false;
  }else{
    document.getElementById('tbChucVu').style.display = "";
    return true;
  }
}

// Kiểm tra số giờ làm
function kiemTraGioLam(value){
  var reg = /^-?\d+\.?\d*$/;
  var isNumber = reg.test(value);
  soGioLam = Number(value);
  if (isNumber) {
    if (soGioLam >= 80 && soGioLam <= 200) {
      document.getElementById("tbGiolam").innerText = "";
      return true;
    } else {
      document.getElementById("tbGiolam").innerText =
        "Giờ làm từ 80 - 200 giờ";
      document.getElementById("tbGiolam").style.display = "block";
      return false;
    }
  }else{
    document.getElementById('tbGiolam').style.display = "block";
    document.getElementById("tbGiolam").innerText =
    "Giờ làm chỉ được nhập số";
    return false;
  }
}
